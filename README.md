track configuration files - copied from https://www.atlassian.com/git/tutorials/dotfiles

the source site has all the spaces replaced with non breaking spaces so makes copy/pasting into bash problematic.

I've fixed that and made a few of my own special modifications.


on the first system, run the cfg-init.sh

on additional systems run cfg-additional-system-setup.sh

