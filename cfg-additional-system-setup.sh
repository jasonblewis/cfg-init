#!/bin/bash

# make sure alias works:
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# ignore the .cfg directory
echo .cfg >> "$HOME/.gitignore"

# clone the repo
git clone --bare "git@gitlab.com:jasonblewis/cfg.git" "$HOME/.cfg"

# Define the alias in the current shell scope: - this seem superfluous?
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

config checkout

echo "If you encounter errors from the checkout, move away the existing files then re-checkout"



